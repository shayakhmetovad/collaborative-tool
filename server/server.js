const express = require("express");
require("express-async-errors");
const morgan = require("morgan");
require("dotenv").config();
const bodyParser = require("body-parser");
const fs = require("fs");
const http = require("http");
const cors = require("cors");
const { save, get } = require("./controllers/chat");
const leaveRoom = require("./utils/leave-room"); // Add this
const { errorHandler } = require("./middlewares/error");
require("./db");
const Document = require("./Document");
const app = express();
app.use(morgan("dev"));
app.use(express.json());
app.use(cors());
// app.use(
// 	bodyParser.urlencoded({
// 		// to support URL-encoded bodies
// 		limit: "150mb",
// 		extended: true,
// 	})
// );
// app.use(bodyParser.json()); //чтобы разархивировать входящие данные запроса, если он заархивирован
fs.readdirSync("./routes").map((r) =>
	app.use("/api", require(`./routes/${r}`))
);
app.use(errorHandler);
const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`Server is running on port ${port}`));

// Create an io server and allow for CORS from http://localhost:3000 with GET and POST methods
const io = require("socket.io")(3002, {
	cors: {
		origin: "http://localhost:3000",
		methods: ["GET", "POST"],
	},
});

const CHAT_BOT = "ChatBot";
let chatRoom = ""; // E.g. javascript, node,...
let allUsers = []; // All users in current chat room

const defaultValue = "";

// Listen for when the client connects via socket.io-client
io.on("connection", (socket) => {
	socket.on("get-document", async (documentId) => {
		const document = findOrCreateDocument(documentId);
		socket.join(documentId);
		socket.emit("load-document", document.data);
		socket.on("send-changes", (delta) => {
			socket.broadcast.to(documentId).emit("receive-changes", delta);
		});
		socket.on("save-document", async (data) => {
			await Document.findByIdAndUpdate(documentId, { data });
		});
	});

	// Add a user to a room
	socket.on("join_room", (data) => {
		console.log("join_room", data);
		const { username, room } = data; // Data sent from client when join_room event emitted
		socket.join(room); // Join the user to a socket room

		let __createdtime__ = Date.now(); // Current timestamp
		// Send message to all users currently in the room, apart from the user that just joined
		socket.to(room).emit("receive_message", {
			message: `${username} has joined the chat room`,
			username: CHAT_BOT,
			__createdtime__,
		});
		// Send welcome msg to user that just joined chat only
		socket.emit("receive_message", {
			message: `Welcome ${username}`,
			username: CHAT_BOT,
			__createdtime__,
		});
		// Save the new user to the room
		chatRoom = room;
		allUsers.push({ id: socket.id, username, room });
		chatRoomUsers = allUsers.filter((user) => user.room === room);
		socket.to(room).emit("chatroom_users", chatRoomUsers);
		socket.emit("chatroom_users", chatRoomUsers);
		// Get last 100 messages sent in the chat room
		get(room)
			.then((last100Messages) => {
				console.log("latest messages", last100Messages);
				socket.emit("last_100_messages", last100Messages);
			})
			.catch((err) => console.log(err));
	});

	socket.on("send_message", (data) => {
		const { message, username, room, __createdtime__ } = data;
		io.in(room).emit("receive_message", data); // Send to all users in room, including sender
		save(data) // Save message in db
			.then((response) => console.log("message saved"))
			.catch((err) => console.log(err));
	});

	// socket.on("leave_room", (data) => {
	// 	console.log("leave_room", data);
	// 	const { username, room } = data;
	// 	socket.leave(room);
	// 	const __createdtime__ = Date.now();
	// 	// Remove user from memory
	// 	allUsers = leaveRoom(socket.id, allUsers);
	// 	socket.to(room).emit("chatroom_users", allUsers);
	// 	socket.to(room).emit("receive_message", {
	// 		username: CHAT_BOT,
	// 		message: `${username} has left the chat`,
	// 		__createdtime__,
	// 	});
	// 	console.log(`${username} has left the chat`);
	// });

	socket.on("disconnect", () => {
		console.log("User disconnected from the chat");
		const user = allUsers.find((user) => user.id == socket.id);
		if (user?.username) {
			allUsers = leaveRoom(socket.id, allUsers);
			socket.to(chatRoom).emit("chatroom_users", allUsers);
			socket.to(chatRoom).emit("receive_message", {
				message: `${user.username} has disconnected from the chat.`,
			});
		}
	});
});
async function findOrCreateDocument(id) {
	if (id == null) return;
	const document = await Document.findById(id);
	if (document) return document;
	return await Document.create({ _id: id, data: defaultValue });
}
