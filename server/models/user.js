const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const {
	generateFromEmail,
	generateUsername,
} = require("unique-username-generator");
const userSchema = mongoose.Schema(
	{
		username: {
			type: String,
			trim: true,
			unique: true,
		},
		name: {
			type: String,
			trim: true,
			required: true,
		},
		surname: {
			type: String,
			trim: true,
			required: true,
		},
		email: {
			type: String,
			trim: true,
			required: true,
			index: true,
			unique: true,
		},
		role: {
			type: String,
			default: "user",
		},
		password: {
			type: String,
			required: true,
		},
	},
	{ timestamps: true }
);

userSchema.pre("save", async function (next) {
	this.username = generateUsername("", 3);
	if (this.isModified("password")) {
		this.password = await bcrypt.hash(this.password, 10);
	}
	next();
});
userSchema.methods.comparePassword = async function (password) {
	const result = await bcrypt.compare(password, this.password);
	return result;
};
module.exports = User = mongoose.model("User", userSchema);
