const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
const fileSchema = new mongoose.Schema(
	{
		title: {
			type: String,
			trim: true,
			required: true,
			unique: false,
		},
		description: {
			type: String,
			required: true,
			maxlength: 2000,
			text: true,
			unique: false,
		},
		categories: [
			{
				type: ObjectId,
				ref: "Category",
			},
		],
		createdBy: { type: ObjectId, ref: "User" },
		// adviserCheckedDate: {
		// 	type: Boolean,
		// 	default: false,
		// },
		// checkedByExpert: {
		// 	type: Boolean,
		// 	default: false,
		// },
		// checkedByEditor: {
		// 	type: Boolean,
		// 	default: false,
		// },
		// isFinished: {
		// 	type: Boolean,
		// 	default: false,
		// },
		status: {
			type: String,
			default: "created",
		},
	},
	{ timestamps: true }
);
module.exports = mongoose.model("File", fileSchema);
