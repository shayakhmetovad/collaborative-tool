const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
const chatSchema = new mongoose.Schema(
	{
		message: {
			type: String,
			required: true,
			unique: false,
			text: true,
		},
		username: { type: String },
		room: { type: String },
		__createdtime__: { type: Number },
	},
	{ timestamps: true }
);
module.exports = mongoose.model("Chat", chatSchema);
