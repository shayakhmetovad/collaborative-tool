const jwt = require("jsonwebtoken");
const User = require("../models/user");
const { sendError } = require("../utils/helper");

exports.authCheck = async (req, res, next) => {
	const token = req.headers?.authorization.replace(/^JWT\s/, "");
	if (!token) return sendError(res, "Invalid token!");
	const jwtToken = token.split(", Bearer ")[1];
	if (!jwtToken) return sendError(res, "Invalid token!");
	const decode = jwt.verify(jwtToken, process.env.JWT_SECRET);
	const { userId } = decode;

	const user = await User.findById(userId);
	if (!user) return sendError(res, "Вы не авторизованы!");

	req.user = user;
	next();
};
exports.adminCheck = async (req, res, next) => {
	const { user } = req;
	if (user.role !== "admin") return sendError(res, "У вас нет прав админа!");
	next();
};
exports.adviserCheck = async (req, res, next) => {
	const { user } = req;
	if (user.role !== "adviser")
		return sendError(res, "У вас нет прав эдвайзера!");
	next();
};
exports.editorCheck = async (req, res, next) => {
	const { user } = req;
	if (user.role !== "editor")
		return sendError(res, "У вас нет прав редактора!");
	next();
};
exports.expertCheck = async (req, res, next) => {
	const { user } = req;
	if (user.role !== "expert") return sendError(res, "unauthorized access!");
	next();
};
