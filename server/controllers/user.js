const User = require("../models/user");
const slugify = require("slugify");
exports.list = async (req, res) => {
	res.json(await User.find({}).sort({ createdAt: -1 })).exec();
};
exports.read = async (req, res) => {
	let user = await User.findById(req.params.id).exec();
	res.json(user);
};
exports.update = async (req, res) => {
	try {
		const updated = await User.findOneAndUpdate(
			{ _id: req.params.id },
			req.body,
			{ new: true }
		).exec();
		res.json(updated);
	} catch (err) {
		res.status(400).send("User update failed");
	}
};
exports.remove = async (req, res) => {
	try {
		const deleted = await User.findOneAndDelete({ id: req.params.id });
		res.json(deleted);
	} catch (err) {
		res.status(400).send("User delete failed");
	}
};
