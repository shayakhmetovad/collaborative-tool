const Role = require("../models/role");
const slugify = require("slugify");
exports.create = async (req, res) => {
	try {
		const { name } = await req.body;
		const role = await new Role({ name, slug: slugify(name) }).save();
		res.json(role);
	} catch (err) {
		res.status(400).send("Create role failed");
	}
};
exports.list = async (req, res) => {
	res.json(await Role.find({}).sort({ createdAt: -1 })).exec();
};
exports.read = async (req, res) => {
	let role = await Role.findOne({ slug: req.params.slug }).exec();
	res.json(role);
};
exports.update = async (req, res) => {
	const { name } = req.body;
	try {
		const updated = await Role.findOneAndUpdate(
			{ slug: req.params.slug },
			{ name, slug: slugify(name) },
			{ new: true }
		);
		res.json(updated);
	} catch (err) {
		res.status(400).send("Role update failed");
	}
};
exports.remove = async (req, res) => {
	try {
		const deleted = await Role.findOneAndDelete({ slug: req.params.slug });
		res.json(deleted);
	} catch (err) {
		res.status(400).send("Role delete failed");
	}
};
