const slugify = require("slugify");
const User = require("../models/user");
const File = require("../models/file");
const Document = require("../Document");
const Chat = require("../models/chat");
exports.create = async (req, res) => {
	try {
		const user = await User.findOne({ email: req.user.email }).exec();
		const { title, description, categories } = req.body;
		// console.log(slugify(title), title, description, categories, user._id);
		const newDocument = await new File({
			title,
			description,
			categories,
			createdBy: user._id,
		}).save();

		res.json(newDocument);
	} catch (err) {
		res.status(400).json({
			err: err.message,
		});
	}
};
exports.list = async (req, res) => {
	let files = await File.find({})
		.populate("categories")
		.populate("createdBy")
		.sort([["createdAt", "desc"]])
		.exec();
	res.json(files);
};
exports.listFileByStatus = async (req, res) => {
	console.log(req.params);
	let files = await File.find({ status: req.params.status })
		.populate("categories")
		.populate("createdBy")
		.sort([["createdAt", "desc"]])
		.exec();
	res.json(files);
};

exports.getDoc = async (req, res) => {
	let doc = await Document.findOne({ _id: req.params.id }).exec();
	console.log(req.params.id);
	res.json(doc);
};
exports.getMessages = async (req, res) => {
	let messages = await Chat.find({ room: req.params.id }).exec();
	console.log(messages);
	res.json(messages);
};
