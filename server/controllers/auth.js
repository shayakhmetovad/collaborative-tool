const User = require("../models/user");
const jwt = require("jsonwebtoken");
const { sendError, generateRandomByte } = require("../utils/helper");
exports.createOrUpdateUser = async (req, res) => {
	const { username, email } = req.user;
	const user = await User.findOneAndUpdate(
		{ email: email },
		{ username: username },
		{ new: true }
	);

	if (user) {
		console.log("Updated user ", user);
		res.json(user);
	} else {
		const newUser = await new User({
			email,
			username: email.split("@")[0],
		}).save();
		console.log("Created user ", newUser);
		res.json(newUser);
	}
};
exports.currentUser = async (req, res) => {
	User.findOne({ email: req.user.email }).exec((err, user) => {
		if (err) throw new Error(err);
		res.json(user);
	});
};
exports.register = async (req, res) => {
	const { username, name, surname, email, password } = req.body;
	const oldUser = await User.findOne({ email }); // returns user data with that email
	if (oldUser) return sendError(res, "This email is already in use");
	const newUser = new User({ username, name, surname, email, password }); //creates id
	await newUser.save(); //saves in db

	try {
		return res.json({ ok: true });
	} catch (err) {
		return res.status(400).send("Error. Try again");
	}
};
exports.login = async (req, res) => {
	const { email, password } = req.body;
	const user = await User.findOne({ email });
	if (!user) return sendError(res, "Email/Password mismatch!");

	const matched = await user.comparePassword(password);
	if (!matched) return sendError(res, "Email/Password mismatch!");
	const { _id, name, role } = user;
	const jwtToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
		expiresIn: "1d",
	});
	res.json({ user: { id: _id, name, email, token: jwtToken, role } });
};
