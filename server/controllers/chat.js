const Chat = require("../models/chat");
exports.save = async (req, res) => {
	try {
		const { message, username, room, __createdtime__ } = await req;
		const chatMessage = await new Chat({
			message,
			username,
			room,
			__createdtime__,
		}).save();
		return chatMessage;
		// res.json(chatMessage);
	} catch (err) {
		console.log(err);
		return err;
	}
};
exports.get = async (req, res) => {
	try {
		const room = await req;
		const chatMessage = await Chat.find({ room }).exec();
		console.log("chatMessage2", chatMessage);
		return chatMessage;
	} catch (err) {
		console.log("error", err);
		// res.status(400).send("Get message failed");
	}
};
