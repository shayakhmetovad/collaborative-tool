const express = require("express");
const router = express.Router();
// middlewares
const {
	authCheck,
	adminCheck,
	editorCheck,
	expertCheck,
	adviserCheck,
} = require("../middlewares/auth");
// import
const {
	create,
	list,
	listFileByStatus,
	getDoc,
	getMessages,
} = require("../controllers/file");
// routes
router.post("/file", authCheck, create);
router.get("/files", authCheck, list);
router.get("/filesByStatus/:status", authCheck, listFileByStatus);
router.get("/doc/:id", getDoc);
router.get("/messages/:id", getMessages);
module.exports = router;
