const express = require("express");

const router = express.Router();

// middlewares
const {
	authCheck,
	adminCheck,
	editorCheck,
	expertCheck,
	adviserCheck,
} = require("../middlewares/auth");
const {
	userValidator,
	validate,
	validatePassword,
	signInValidator,
} = require("../middlewares/validator");
// controllers
const {
	register,
	login,
	createOrUpdateUser,
	currentUser,
} = require("../controllers/auth");
// router.get('/:message', showMessage);
router.post("/register", userValidator, validate, register);
router.post("/login", signInValidator, validate, login);
router.post("/create-or-update-user", authCheck, createOrUpdateUser);
router.post("/current-user", authCheck, currentUser);
router.post("/current-admin", authCheck, adminCheck, currentUser);
router.post("/current-expert", authCheck, expertCheck, currentUser);
router.post("/current-editor", authCheck, editorCheck, currentUser);
router.post("/current-adviser", authCheck, adviserCheck, currentUser);
module.exports = router;
