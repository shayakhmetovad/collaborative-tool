const express = require("express");
const router = express.Router();

// middlewares
const { authCheck, adminCheck } = require("../middlewares/auth");

// import
const { create, update, read, remove, list } = require("../controllers/role");

// routes
router.post("/role", authCheck, adminCheck, create);
router.get("/roles", list);
router.get("/role/:slug", read);
router.put("/role/:slug", authCheck, adminCheck, update);
router.delete("/role/:slug", authCheck, adminCheck, remove);
module.exports = router;
