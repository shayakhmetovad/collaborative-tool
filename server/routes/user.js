const express = require("express");
const router = express.Router();

// middlewares
const { authCheck, adminCheck } = require("../middlewares/auth");

// import
const { update, read, remove, list } = require("../controllers/user");

// routes
router.get("/users", list);
router.get("/user/:id", read);
router.put("/user/:slug", authCheck, adminCheck, update);
router.delete("/user/:slug", authCheck, adminCheck, remove);
module.exports = router;
