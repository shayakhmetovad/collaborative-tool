import React, { Suspense } from "react";
import { useDispatch } from "react-redux";
import {
	BrowserRouter as Router,
	Routes,
	Route,
	Navigate,
} from "react-router-dom";
import jwt_decode from "jwt-decode";
import { v4 as uuidV4 } from "uuid";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { LoadingOutlined } from "@ant-design/icons";
import setAuthToken from "utils/setAuthToken";
import store from "store";
import { logoutUser, setCurrentUser } from "./functions/auth";
import TextEditor from "./TextEditor";
import Login from "./pages/auth/Login";
import RegisterGoogle from "./pages/auth/RegisterGoogle";
import Register from "./pages/auth/Register";
import RegisterComplete from "./pages/auth/RegisterComplete";
import Header from "./components/nav/Header";
import Main from "pages/Main";
import CreateDocument from "pages/CreateDocument";
import Antiplagiarism from "pages/Antiplagiarism";
import Summarize from "pages/Summarize";
import GenerateText from "pages/GenerateText";
import CategoryList from "components/category/CategoryList";
import AdminRoute from "components/routes/AdminRoute";
import AdminDashboard from "pages/admin/AdminDashboard";
import CategoryCreate from "pages/admin/category/CategoryCreate";
import CategoryUpdate from "pages/admin/category/CategoryUpdate";
import UserRoute from "components/routes/UserRoute";
import UsersList from "pages/admin/users/UsersList";
import RolesList from "pages/admin/roles/RolesList";
import RoleUpdate from "pages/admin/roles/RoleUpdate";
import UserUpdate from "pages/admin/users/UserUpdate";
import Dashboard from "pages/Dashboard";
import Reference from "pages/Reference";

function App() {
	const dispatch = useDispatch();
	if (localStorage.token) {
		setAuthToken(localStorage.token);
		const decoded = jwt_decode(localStorage.token);
		store.dispatch(setCurrentUser(JSON.parse(localStorage.auth)));
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			store.dispatch(logoutUser());
			window.location.href = "/";
		}
	}
	return (
		<Suspense
			fallback={
				<div className="col text-center p-5">
					<LoadingOutlined />
				</div>
			}
		>
			<ToastContainer position="top-center" />
			<Router>
				<Header />
				<Routes>
					<Route path="/home" exact element={<Main />} />
					<Route path="/" exact element={<Dashboard />} />
					<Route path="/login" exact element={<Login />} />
					<Route path="/register_google" exact element={<RegisterGoogle />} />
					<Route path="/register" exact element={<Register />} />
					<Route path="/reference" exact element={<Reference />} />
					<Route
						path="/register/complete"
						exact
						element={<RegisterComplete />}
					/>

					<Route path="/documents/:id" element={<TextEditor />} />
					<Route
						path="/create-document"
						element={<UserRoute Component={CreateDocument} />}
					/>
					<Route
						path="/antiplagiarism"
						element={<UserRoute Component={Antiplagiarism} />}
					/>
					<Route
						path="/summary"
						element={<UserRoute Component={Summarize} />}
					/>
					<Route
						path="/generate-text"
						element={<UserRoute Component={GenerateText} />}
					/>
					<Route path="/categories" component={<CategoryList />} exact />
					<Route
						path="/admin/dashboard"
						exact
						element={<AdminRoute Component={AdminDashboard} />}
					/>
					<Route
						path="/admin/category"
						exact
						element={<AdminRoute Component={CategoryCreate} />}
					/>
					<Route
						path="/admin/category/:slug"
						exact
						element={<AdminRoute Component={CategoryUpdate} />}
					/>
					<Route
						path="/admin/users"
						exact
						element={<AdminRoute Component={UsersList} />}
					/>
					<Route
						path="/admin/user/:id"
						exact
						element={<AdminRoute Component={UserUpdate} />}
					/>
					<Route
						path="/admin/roles"
						exact
						element={<AdminRoute Component={RolesList} />}
					/>
					<Route
						path="/admin/role/:slug"
						exact
						element={<AdminRoute Component={RoleUpdate} />}
					/>
				</Routes>
			</Router>
		</Suspense>
	);
}

export default App;
