export class User {
	createdAt?: string;
	email?: string;
	updatedAt?: string;
	username?: string;
	_id?: string;
}
export class UserToken {
	token?: string;
	user: User = new User();
}
