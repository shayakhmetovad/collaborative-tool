import React, { useCallback, useEffect, useRef, useState } from "react";
import Quill from "quill";
import "quill/dist/quill.snow.css";
import { io } from "socket.io-client";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import FunctionalMenu from "components/nav/FunctionalMenu";
import ChatRoom from "components/nav/ChatRoom";
import { getDoc } from "functions/document";
import isEmpty from "validation/is-empty";

const SAVE_INTERVAL_MS = 500;
const TOOLBAR_OPTIONS = [
	[{ header: [1, 2, 3, 4, 5, 6, false] }],
	[{ font: [] }],
	[{ list: "ordered" }, { list: "bullet" }],
	["bold", "italic", "underline"],
	[{ color: [] }, { background: [] }],
	[{ script: "sub" }, { script: "super" }],
	[{ align: [] }],
	["image", "blockquote", "code-block"],
	["clean"],
];
export default function TextEditor() {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const { id: documentId } = useParams();
	const [socket, setSocket] = useState();
	const [quill, setQuill] = useState();
	const [docData, setDocData] = useState();

	// Connect to server
	useEffect(() => {
		getDoc(documentId).then((doc) => {
			setDocData(doc.data.data.ops);
		});
		console.log();
		const s = io("http://localhost:3002");
		setSocket(s);
		return () => {
			s.disconnect();
		};
	}, []);
	useEffect(() => {
		getDoc(documentId).then((doc) => {
			setDocData(doc.data.data.ops);
		});
	}, [socket, quill, documentId]);
	useEffect(() => {
		if (socket == null || quill == null) return;

		const interval = setInterval(() => {
			if (isEmpty(quill.getContents().ops[0].insert)) {
				getDoc(documentId).then((doc) => {
					quill.setContents(doc.data.data.ops);
				});

				return;
			}
			socket.emit("save-document", quill.getContents());
		}, SAVE_INTERVAL_MS);
		return () => {
			clearInterval(interval);
		};
	}, [socket, quill]);
	useEffect(() => {
		if (socket == null || quill == null) return;
		socket.once("load-document", (document) => {
			quill.setContents(document);
			quill.enable();
		});
		socket.emit("get-document", documentId);
	}, [socket, quill, documentId]);

	useEffect(() => {
		if (socket == null || quill == null) return;
		const handler = (delta, oldDelta, source) => {
			if (source !== "user") return;
			socket.emit("send-changes", delta);
		};
		quill.on("text-change", handler);
		return () => {
			quill.off("text-change", handler);
		};
	}, [socket, quill]);
	useEffect(() => {
		if (socket == null || quill == null) return;
		const handler = (delta) => {
			quill.updateContents(delta);
		};
		socket.on("receive-changes", handler);
		return () => {
			socket.off("receive-changes", handler);
		};
	}, [socket, quill]);
	const wrapperRef = useCallback((wrapper) => {
		if (wrapper == null) return;
		wrapper.innerHTML = "";
		const editor = document.createElement("div");
		editor.innerHtml = docData;
		wrapper.append(editor);
		const q = new Quill(editor, {
			theme: "snow",
			modules: { toolbar: TOOLBAR_OPTIONS },
		});
		q.disable();
		q.setText("Loading...");
		q.setContents(docData);
		setQuill(q);
	}, []);
	return (
		<div className="d-flex" style={{ position: "relative" }}>
			<FunctionalMenu />
			<div className="container" ref={wrapperRef}></div>
			<ChatRoom username={user.email} room={documentId} />
		</div>
	);
}
