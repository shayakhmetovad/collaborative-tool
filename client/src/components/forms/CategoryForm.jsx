import React from "react";

const CategoryForm = ({ handleSubmit, name, setName, buttonName }) => (
	<form onSubmit={handleSubmit}>
		<div className="form-group">
			<label>Название</label>
			<input
				type="text"
				className="form-control"
				value={name}
				onChange={(e) => setName(e.target.value)}
				autoFocus
				required
			/>
			<br />
			<button type="submit" className="btn btn-outline-primary">
				{buttonName}
			</button>
		</div>
	</form>
);
export default CategoryForm;
