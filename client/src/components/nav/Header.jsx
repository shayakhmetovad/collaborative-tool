import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Menu } from "antd";
import {
	AppstoreOutlined,
	SettingOutlined,
	UserOutlined,
	UserAddOutlined,
	LogoutOutlined,
	BookOutlined,
} from "@ant-design/icons";
import { logoutUser } from "functions/auth";

const { SubMenu, Item } = Menu;

const Header = () => {
	let dispatch = useDispatch();
	let navigate = useNavigate();

	let { auth } = useSelector((state) => ({ ...state }));
	const { isAuthenticated, user } = auth;
	const [current, setCurrent] = useState("home");

	const handleClick = (e) => {
		setCurrent(e.key);
	};

	const logout = () => {
		dispatch(logoutUser());
		navigate("/");
	};

	return (
		<Menu
			className="mb-3"
			onClick={handleClick}
			selectedKeys={[current]}
			mode="horizontal"
		>
			<div className="container d-flex p-0">
				<Item key="home" icon={<AppstoreOutlined />}>
					<Link to="/">Главная</Link>
				</Item>
				{isAuthenticated && (
					<>
						<Item icon={<BookOutlined />}>
							<Link to="/home">Материалы</Link>
						</Item>
						<SubMenu
							key="SubMenu"
							icon={<SettingOutlined />}
							title={user.email}
						>
							{user.role === "subscriber" && (
								<Item>
									<Link to="/user/history">Subscriber</Link>
								</Item>
							)}
							{user.role === "admin" && (
								<Item>
									<Link to="/admin/dashboard">Админ</Link>
								</Item>
							)}

							<Item icon={<LogoutOutlined />} onClick={logout}>
								Выход
							</Item>
						</SubMenu>
					</>
				)}
				{!isAuthenticated && (
					<Item key="register" icon={<UserAddOutlined />}>
						<Link to="/register">Регистрация</Link>
					</Item>
				)}
				{!isAuthenticated && (
					<Item key="login" icon={<UserOutlined />}>
						<Link to="/login">Вход</Link>
					</Item>
				)}
			</div>
		</Menu>
	);
};
export default Header;
