import React, { useEffect, useState } from "react";
import { Link, Navigate, useLocation, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { MailOutlined } from "@ant-design/icons";
import { Menu } from "antd";
function getItem(label, key, icon, children) {
	return {
		key,
		icon,
		children,
		label,
	};
}
const items = [
	getItem(<Link to="/">Главная</Link>, "1", <MailOutlined />),
	getItem(<Link to="/reference">Справка</Link>, "2", <MailOutlined />),
];
const UserNav = () => {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const navigate = useNavigate();
	const location = useLocation();
	useEffect(() => {
		if (location.pathname === "/reference") {
		} else {
		}
	}, []);

	return (
		<Menu defaultSelectedKeys={["1"]} defaultOpenKeys={["1"]} items={items} />
	);
};
export default UserNav;
