import {
	AppstoreOutlined,
	ContainerOutlined,
	MenuFoldOutlined,
	PieChartOutlined,
	DesktopOutlined,
	MailOutlined,
	MenuUnfoldOutlined,
} from "@ant-design/icons";
import { Link, Navigate, useLocation, useNavigate } from "react-router-dom";
import { Button, Menu } from "antd";
import { useState } from "react";
function getItem(label, key, icon, children, type) {
	return {
		key,
		icon,
		children,
		label,
		type,
	};
}
const items = [
	getItem("Литература", "1", <PieChartOutlined />),
	getItem(
		<Link to="/antiplagiarism">Антиплагиат</Link>,
		"2",
		<DesktopOutlined />
	),
	getItem(
		<Link to="/summary">Текстовое резюме</Link>,
		"3",
		<ContainerOutlined />
	),
	getItem(
		<Link to="/generate-text">Генерация картинки</Link>,
		"4",
		<ContainerOutlined />
	),
];
const App = () => {
	const [collapsed, setCollapsed] = useState(false);
	const toggleCollapsed = () => {
		setCollapsed(!collapsed);
	};
	return (
		<div
			style={{
				width: 256,
			}}
		>
			<Button
				type="primary"
				onClick={toggleCollapsed}
				style={{
					marginBottom: 16,
				}}
			>
				{collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
			</Button>
			<Menu
				defaultSelectedKeys={["1"]}
				defaultOpenKeys={["sub1"]}
				mode="inline"
				inlineCollapsed={collapsed}
				items={items}
			/>
		</div>
	);
};
export default App;
