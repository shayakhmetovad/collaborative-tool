import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useState, useEffect, useRef } from "react";
import { getMessages } from "functions/document";
import "./chat.scss";
import io from "socket.io-client";
const ChatRoom = ({ username, room }) => {
	const [collapsed, setCollapsed] = useState(false);
	const [socket, setSocket] = useState();
	const [messagesRecieved, setMessagesReceived] = useState([]);
	const messagesColumnRef = useRef(null);
	const [message, setMessage] = useState("");
	const [sended, setSended] = useState(false);
	const [messages, setMessages] = useState();
	// Connect to server
	useEffect(() => {
		getMessages(room).then((msgs) => {
			console.log("msd", msgs.data);
			setMessagesReceived(msgs.data);
		});
		const s = io("http://localhost:3002");
		setSocket(s);
		return () => {
			s.disconnect();
		};
	}, []);
	// Runs whenever a socket event is recieved from the server
	useEffect(() => {
		if (socket == null) return;
		if (room !== "" && username !== "") {
			socket.emit("join_room", { username, room });
		}

		socket.on("receive_message", (data) => {
			setMessagesReceived((state) => [
				...state,
				{
					message: data.message,
					username: data.username,
					__createdtime__: data.__createdtime__,
				},
			]);
		});
		// Remove event listener on component unmount
		return () => socket.off("receive_message");
	}, [socket]);

	useEffect(() => {
		if (socket == null) return;
		// Last 100 messages sent in the chat room (fetched from the db in backend)
		socket.on("last_100_messages", (last100Messages) => {
			console.log("Last 100 messages JSON:", JSON.parse(last100Messages));
			console.log("Last 100 messages:", last100Messages);
			last100Messages = JSON.parse(last100Messages);
			// Sort these messages by __createdtime__
			last100Messages = sortMessagesByDate(last100Messages);
			setMessagesReceived((state) => [...last100Messages, ...state]);
		});

		return () => socket.off("last_100_messages");
	}, [socket]);

	useEffect(() => {
		if (socket == null) return;
		socket.on("chatroom_users", (data) => {
			console.log("chatroom_users", data);
		});
		return () => socket.off("chatroom_users");
	}, [socket]);

	// Scroll to the most recent message
	useEffect(() => {
		messagesColumnRef.current.scrollTop =
			messagesColumnRef.current.scrollHeight;
	}, [messagesRecieved]);

	function sortMessagesByDate(messages) {
		return messages.sort(
			(a, b) => parseInt(a.__createdtime__) - parseInt(b.__createdtime__)
		);
	}

	// dd/mm/yyyy, hh:mm:ss
	function formatDateFromTimestamp(timestamp) {
		const date = new Date(timestamp);
		return date.toLocaleString();
	}

	const toggleCollapsed = () => {
		setCollapsed(!collapsed);
	};

	const sendMessage = (e) => {
		e.preventDefault();
		if (message !== "") {
			const __createdtime__ = Date.now();
			// Send message to server. We can't specify who we send the message to from the frontend. We can only send to server. Server can then send message to rest of users in room
			console.log("data", { username, room, message, __createdtime__ });
			socket.emit("send_message", { username, room, message, __createdtime__ });
			setMessage("");
			setSended(!sended);
		}
	};
	return (
		<div
			className={collapsed ? "chat-expand-div open-expand" : "chat-expand-div"}
			style={{
				display: "flex",
			}}
		>
			<Button
				type="primary"
				className="chat-expand-btn"
				onClick={toggleCollapsed}
			>
				{collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
			</Button>
			<div className={collapsed ? "open-chat chat" : "close-chat chat"}>
				<div>
					<div className="chat-top">
						<p>Беседа</p>
					</div>
					<div className="messagesColumn" ref={messagesColumnRef}>
						{messagesRecieved.map((msg, i) => (
							<>
								<div
									className={
										msg.username == username
											? "msg-left message"
											: "msg-right message"
									}
									key={i}
								>
									<div
										style={{ display: "flex", justifyContent: "space-between" }}
									>
										<span className="msgMeta">{msg.username}</span>
										<span className="msgMeta">
											{formatDateFromTimestamp(msg.__createdtime__)}
										</span>
									</div>
									<p className="msgText">{msg.message}</p>
									<br />
								</div>
							</>
						))}
					</div>
				</div>
				<div className="sendMessageContainer">
					<form onSubmit={sendMessage}>
						<input
							className="messageInput"
							placeholder="Введите текст..."
							onChange={(e) => setMessage(e.target.value)}
							value={message}
						/>
						<button className="btn btn-primary btn-chat-send" type="submit">
							Отправить
						</button>
					</form>
				</div>
			</div>
		</div>
	);
};
export default ChatRoom;
