import React from "react";
import { Link } from "react-router-dom";

const AdminNav = () => (
	<nav>
		<ul className="nav flex-column">
			<li className="nav-item">
				<Link to="/admin/users" className="nav-link">
					Пользователи
				</Link>
			</li>
			<li className="nav-item">
				<Link to="/admin/roles" className="nav-link">
					Роли
				</Link>
			</li>
			<li className="nav-item">
				<Link to="/admin/category" className="nav-link">
					Категория
				</Link>
			</li>
		</ul>
	</nav>
);

export default AdminNav;
