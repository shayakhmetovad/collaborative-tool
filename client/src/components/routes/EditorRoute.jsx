import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import LoadingToRedirect from "./LoadingToRedirect";
import { currentEditor } from "functions/auth";

const EditorRoute = ({ Component }) => {
	const { auth } = useSelector((state) => ({ ...state }));
	const [ok, setOk] = useState(true);

	useEffect(() => {
		currentEditor()
			.then((res) => {
				console.log("Current Editor Res", res);
				setOk(true);
			})
			.catch((err) => {
				console.log("Editor Route Err", err);
				setOk(false);
			});
	}, [auth.user]);

	return ok ? <Component /> : <LoadingToRedirect />;
};

export default EditorRoute;
