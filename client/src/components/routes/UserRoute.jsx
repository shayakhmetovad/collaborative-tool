import React from "react";
import { useSelector } from "react-redux";
import LoadingToRedirect from "./LoadingToRedirect";

const UserRoute = ({ Component }) => {
	const { auth } = useSelector((state) => ({ ...state }));
	return auth.user && auth.user.token ? <Component /> : <LoadingToRedirect />;
};

export default UserRoute;
