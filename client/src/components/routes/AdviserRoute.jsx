import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import LoadingToRedirect from "./LoadingToRedirect";
import { currentAdviser } from "functions/auth";

const AdviserRoute = ({ Component }) => {
	const { auth } = useSelector((state) => ({ ...state }));
	const [ok, setOk] = useState(true);

	useEffect(() => {
		currentAdviser()
			.then((res) => {
				console.log("Current Adviser Res", res);
				setOk(true);
			})
			.catch((err) => {
				console.log("Adviser Route Err", err);
				setOk(false);
			});
	}, [auth.user]);

	return ok ? <Component /> : <LoadingToRedirect />;
};

export default AdviserRoute;
