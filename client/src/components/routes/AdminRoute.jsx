import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import LoadingToRedirect from "./LoadingToRedirect";
import { currentAdmin } from "functions/auth";

const AdminRoute = ({ Component }) => {
	const { auth } = useSelector((state) => ({ ...state }));
	const [ok, setOk] = useState(true);

	useEffect(() => {
		currentAdmin()
			.then((res) => {
				console.log("Current Admin Res", res);
				setOk(true);
			})
			.catch((err) => {
				console.log("Admin Route Err", err);
				setOk(false);
			});
	}, [auth.user]);

	return ok ? <Component /> : <LoadingToRedirect />;
};

export default AdminRoute;
