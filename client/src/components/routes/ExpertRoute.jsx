import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import LoadingToRedirect from "./LoadingToRedirect";
import { currentExpert } from "functions/auth";

const ExpertRoute = ({ Component }) => {
	const { auth } = useSelector((state) => ({ ...state }));
	const [ok, setOk] = useState(true);

	useEffect(() => {
		currentExpert()
			.then((res) => {
				console.log("Current Expert Res", res);
				setOk(true);
			})
			.catch((err) => {
				console.log("Expert Route Err", err);
				setOk(false);
			});
	}, [auth.user]);

	return ok ? <Component /> : <LoadingToRedirect />;
};

export default ExpertRoute;
