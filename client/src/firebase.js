import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
// import { getAnalytics } from "firebase/analytics";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBz3ZJ3nAPjcOXsk8g1saLY_6MIWtQg8aI",
  authDomain: "smartbook-5e4e2.firebaseapp.com",
  projectId: "smartbook-5e4e2",
  storageBucket: "smartbook-5e4e2.appspot.com",
  messagingSenderId: "512887888903",
  appId: "1:512887888903:web:4400ade5349ea19572dea2",
  measurementId: "G-E03QNCLW48"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// const analytics = getAnalytics(app);
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();