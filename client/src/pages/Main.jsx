import React, { useEffect, useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button } from "antd";
import { PlusCircleOutlined, FileWordTwoTone } from "@ant-design/icons";
import { getFiles } from "functions/document";
import AdminDashboard from "./admin/AdminDashboard";
import EditorMain from "./editor/EditorMain";
import AdviserMain from "./adviser/AdviserMain";
import ExpertMain from "./expert/ExpertMain";

export default function Main() {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const [files, setFiles] = useState([]);
	const navigate = useNavigate();
	useEffect(() => {
		console.log(user);
		if (!user) {
			navigate("/");
		}
		getFiles(auth.user.token).then((files) => {
			setFiles(files.data);
			console.log(files.data);
		});
	}, []);

	return (
		<>
			{user?.role === "user" ? (
				<div className="container">
					<div className="d-flex justify-content-between mb-3">
						<h5 className="h5-title">Все документы</h5>
						<Button type="primary">
							<Link to="/create-document" className="d-flex align-items-center">
								<PlusCircleOutlined style={{ marginRight: "10px" }} />
								Добавить документ
							</Link>
						</Button>
					</div>
					<div className="">
						<div className="d-flex justify-content-between">
							<p className="col-1"></p>
							<p className="col-6">Название документа</p>
							<p className="col-3">Создатель документа</p>
							<p className="col-2">Владелец</p>
						</div>
						{files.map((file) => (
							<div className="d-flex justify-content-between" key={file._id}>
								<FileWordTwoTone className="col-1 d-flex" />
								<p className="col-6">
									<Link to={`/documents/${file._id}`}>{file.title}</Link>
								</p>
								<p className="col-3">{file.createdBy.username}</p>
								<p className="col-2">
									{file.createdBy.email === auth.user.email ? "Я" : "Не я"}
								</p>
							</div>
						))}
					</div>
				</div>
			) : user.role === "admin" ? (
				<AdminDashboard />
			) : user.role === "expert" ? (
				<ExpertMain />
			) : user.role === "editor" ? (
				<EditorMain />
			) : (
				<AdviserMain />
			)}
		</>
	);
}
