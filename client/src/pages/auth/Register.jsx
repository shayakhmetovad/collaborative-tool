import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Input } from "antd";
import { toast } from "react-toastify";
import { register } from "../../functions/auth";

const Register = ({ history }) => {
	const [name, setName] = useState("");
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [surname, setSurname] = useState("");
	const [loading, setLoading] = useState(false);
	const navigate = useNavigate();
	const handleSubmit = async (e) => {
		e.preventDefault();

		register({
			name,
			surname,
			email,
			password,
		})
			.then((user) => {
				console.log(user);
				toast.success("Успешно зарегестрированы!");
				navigate("/login");
			})
			.catch((err) => {
				toast.error(err);
			});

		// window.history.pushState({},undefined,'/login');
	};

	return (
		<div className="container p-5">
			<div className="row">
				<div className="col-md-6 offset-md-3">
					{loading ? (
						<h4 className="text-danger text-center">Загрузка...</h4>
					) : (
						<h4 className="text-center mb-4">Регистрация</h4>
					)}

					<form onSubmit={handleSubmit}>
						{/* <Input
							placeholder="Введите имя"
							value={username}
							onChange={(e) => setUsername(e.target.value)}
							autoFocus
						/> */}
						<Input
							placeholder="Введите имя"
							value={name}
							onChange={(e) => setName(e.target.value)}
							autoFocus
						/>
						<Input
							placeholder="Введите фамилию"
							value={surname}
							onChange={(e) => setSurname(e.target.value)}
							className="mt-3"
						/>
						<Input
							placeholder="Введите почту"
							type="email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
							className="mt-3"
						/>
						<Input.Password
							placeholder="Введите пароль"
							type="password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							className="mt-3"
						/>
						{/* <Input.Password
							placeholder="повторите пароль"
							type="password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							className="mt-3"
						/> */}
						<Button
							type="primary"
							block
							shape="round"
							className="mb-3 mt-3"
							size="large"
							htmlType="submit"
							disabled={!email || password.length < 6}
						>
							Регистрация
						</Button>
					</form>
					{/* <Link to="/forgot/password" className="float-right text-danger">
						Забыли пароль?
					</Link> */}
				</div>
			</div>
		</div>
	);
};

export default Register;
