import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { Button, Input } from "antd";
import { login, setCurrentUser } from "../../functions/auth";
import setAuthToken from "utils/setAuthToken";

const Login = ({ history }) => {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [loading, setLoading] = useState(false);
	const { user } = useSelector((state: any) => ({ ...state }));
	let navigate = useNavigate();
	useEffect(() => {
		if (window.history.state) {
			return;
		} else {
			if (user && user.token) {
				navigate("/home");
			}
		}
	}, [user, navigate]);

	let dispatch = useDispatch();

	const handleSubmit = async (e) => {
		e.preventDefault();
		setLoading(true);
		login({ email, password })
			.then((res) => {
				setLoading(false);
				const { email, id, name, token } = res.data.user;
				localStorage.setItem("token", JSON.stringify(token));
				localStorage.setItem("auth", JSON.stringify(res.data.user));
				setAuthToken(token);
				dispatch(setCurrentUser(res.data.user));
				setLoading(false);
				toast.success("Успешно авторизовались!");
				navigate("/home");
			})
			.catch((err) => {
				toast.error(`${err.response.data.error}`);
				setLoading(false);
			});
	};

	return (
		<div className="container p-5">
			<div className="row">
				<div className="col-md-6 offset-md-3">
					{loading ? (
						<h4 className="text-danger text-center">Загрузка...</h4>
					) : (
						<h4 className="text-center mb-4">Вход</h4>
					)}

					<form onSubmit={handleSubmit}>
						<Input
							placeholder="Введите почту"
							type="email"
							value={email}
							onChange={(e) => setEmail(e.target.value)}
							autoFocus
						/>
						<Input.Password
							placeholder="Введите пароль"
							type="password"
							value={password}
							onChange={(e) => setPassword(e.target.value)}
							className="mt-3"
						/>
						<Button
							type="primary"
							block
							shape="round"
							className="mb-3 mt-3"
							size="large"
							htmlType="submit"
							disabled={!email || password.length < 6}
						>
							Войти
						</Button>
					</form>
					<Link to="/forgot/password" className="float-right text-danger">
						Забыли пароль?
					</Link>
				</div>
			</div>
		</div>
	);
};
export default Login;
