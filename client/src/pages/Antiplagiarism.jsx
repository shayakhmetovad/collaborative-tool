import React, { useEffect, useState } from "react";
import { Button, Input, Typography } from "antd";
import { sendText } from "../api/textApi";
import "./inputText.scss";
import FunctionalMenu from "components/nav/FunctionalMenu";
const { TextArea } = Input;
const { Title } = Typography;

const InputText = () => {
	const [loading, setLoading] = useState(false);
	const [text, setText] = useState("");
	const [report, setReport] = useState({});
	const [reportFetched, setReportFetched] = useState(false);

	useEffect(() => {
		setReportFetched(false);
		setReport({});
	}, []);
	function inputChangeHandle(event) {
		setText(event.target.value);
	}
	async function submitHandler() {
		setLoading(true);
		var data = await sendText(text);
		setReport(data);
		setReportFetched(true);
		setLoading(false);
		console.log(data);
	}
	var loader = (
		<div className="loader">
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
		</div>
	);

	return (
		<div className="d-flex">
			<FunctionalMenu />

			<div className="container">
				<Title level={2}>Проверка на плагиат</Title>

				<TextArea
					rows={10}
					placeholder="Введите текст"
					value={text}
					onChange={inputChangeHandle}
				/>
				<br />
				<br />
				{!loading && (
					<Button
						size="large"
						type="primary"
						disabled={!text}
						className="submitButton"
						onClick={submitHandler}
					>
						Проверить
					</Button>
				)}
				{loading && (
					<Button type="primary" className="submitButton">
						{loader}
					</Button>
				)}
				{reportFetched && (
					<div className="Report">
						<h2 style={{ color: "red" }}>
							Плагиат : {report.percentPlagiarism}%
						</h2>
						<h2>Уникальность : {100 - report.percentPlagiarism}%</h2>
					</div>
				)}
			</div>
		</div>
	);
};

export default InputText;
