import React, { useEffect, useState } from "react";
import { Button, Input, Typography } from "antd";
import axios from "axios";
import { sendText } from "../api/textApi";
import "./inputText.scss";
import FunctionalMenu from "components/nav/FunctionalMenu";
const { TextArea } = Input;
const { Title } = Typography;

const InputText = () => {
	const [loading, setLoading] = useState(false);
	const [text, setText] = useState("");
	const [summary, setSummary] = useState("");
	const [error, setError] = useState("");
	const [reportFetched, setReportFetched] = useState(false);

	useEffect(() => {
		setReportFetched(false);
		setSummary("");
	}, []);
	function inputChangeHandle(event) {
		setText(event.target.value);
	}
	const handleSubmit = async (e) => {
		e.preventDefault();
		setLoading(true);
		try {
			const { data } = await axios.post(
				`${process.env.REACT_APP_API}/summary`,
				{ text }
			);
			setSummary(data);
		} catch (err) {
			if (err.response.data.error) {
				setError(err.response.data.error);
			} else if (err.message) {
				setError(err.message);
			}
			setTimeout(() => {
				setError("");
			}, 5000);
		}
		setReportFetched(true);
		setLoading(false);
	};
	var loader = (
		<div className="loader">
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
		</div>
	);

	return (
		<div className="d-flex">
			<FunctionalMenu />

			<div className="container">
				<form onSubmit={handleSubmit}>
					<Title level={2}>Суммировать текст</Title>

					<TextArea
						rows={10}
						placeholder="Введите текст"
						value={text}
						onChange={inputChangeHandle}
					/>
					<br />
					<br />
					{!loading && (
						<Button
							size="large"
							type="primary"
							disabled={!text}
							htmlType="submit"
							className="submitButton"
							// onClick={handleSubmit}
						>
							Проверить
						</Button>
					)}
					{loading && (
						<Button type="primary" className="submitButton">
							{loader}
						</Button>
					)}
					{reportFetched && summary && (
						<Typography p={2}>Итог : {summary}</Typography>
					)}
				</form>
			</div>
		</div>
	);
};

export default InputText;
