import React, { useEffect, useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { MailOutlined } from "@ant-design/icons";
import { Menu } from "antd";
import UserNav from "components/nav/UserNav";
function getItem(label, key, icon, children) {
	return {
		key,
		icon,
		children,
		label,
	};
}
const items = [
	getItem("Navigation One", "1", <MailOutlined />),
	getItem(
		<a href="https://ant.design" target="_blank" rel="noopener noreferrer">
			Справка
		</a>,
		"link",
		<MailOutlined />
	),
];
const Dashboard = () => {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const navigate = useNavigate();

	useEffect(() => {}, []);

	return (
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<UserNav />
				</div>
				<div className="col-md-10">
					<h1>Добро пожаловать в CollabBook</h1>
					<p>
						Здесь в CollabBook совместно пишется образовательная литература,
						распространяемая свободно и доступная всем.
					</p>
					<p>
						Любой желающий может дополнить имеющиеся учебники или начать свой.
					</p>
					<p>
						Надеемся, вы получите большое удовольствие от участия в проекте.
						Обратите внимание на основные принципы участия: правьте смело и
						предполагайте добрые намерения. Если у вас возникли вопросы,
						воспользуйтесь <Link to="/reference">справкой</Link>. Если вы не
						нашли в ней ответа на ваш вопрос, задайте его на форуме проекта. И
						ещё раз, добро пожаловать!
					</p>
				</div>
			</div>
		</div>
	);
};
export default Dashboard;
