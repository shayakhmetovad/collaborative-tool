import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { toast } from "react-toastify";
import AdminNav from "components/nav/AdminNav";
import { updateRole, getRole } from "../../../functions/role";
import CategoryForm from "../../../components/forms/CategoryForm";

const RoleUpdate = () => {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const [name, setName] = useState("");
	const [loading, setLoading] = useState(false);
	const { slug } = useParams();
	const navigate = useNavigate();
	useEffect(() => {
		loadRole();
	}, []);
	const loadRole = () => getRole(slug).then((c) => setName(c.data.name));

	const handleSubmit = (e) => {
		e.preventDefault();
		setLoading(true);
		updateRole(slug, { name }, user.token)
			.then((res) => {
				setLoading(false);
				setName("");
				toast.success(`Роль ${res.data.name} успешно обновлен!`);
				navigate("/admin/roles");
			})
			.catch((err) => {
				setLoading(false);
				if (err.response.status === 400) toast.error(err.response.data);
			});
	};

	return (
		<div className="container-fluid">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col">
					{loading ? (
						<h4 className="text-danger">Загрузка...</h4>
					) : (
						<h4>Обновление названия</h4>
					)}

					<CategoryForm
						handleSubmit={handleSubmit}
						name={name}
						setName={setName}
						buttonName="Сохранить"
					/>

					<hr />
				</div>
			</div>
		</div>
	);
};
export default RoleUpdate;
