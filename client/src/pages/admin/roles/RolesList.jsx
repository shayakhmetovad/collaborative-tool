import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import AdminNav from "components/nav/AdminNav";
import { createRole, getRoles, removeRole } from "../../../functions/role";
import CategoryForm from "components/forms/CategoryForm";
import LocalSearch from "components/forms/LocalSearch";

export default function RolesList() {
	const { auth } = useSelector((state) => ({ ...state }));
	const [name, setName] = useState("");
	const [loading, setLoading] = useState(false);
	const [roles, setRoles] = useState([]);

	const [keyword, setKeyword] = useState("");

	useEffect(() => {
		loadRoles();
	}, []);
	const loadRoles = () => getRoles().then((c) => setRoles(c.data));

	const handleSubmit = (e) => {
		e.preventDefault();
		setLoading(true);
		createRole({ name }, auth.user.token)
			.then((res) => {
				setLoading(false);
				setName("");
				toast.success(`Роль ${res.data.name} успешно создан!`);
				loadRoles();
			})
			.catch((err) => {
				console.log(err);
				setLoading(false);
				if (err.response.status === 400) toast.error(err.response.data);
			});
	};
	const handleRemove = async (slug) => {
		setLoading(true);
		let answer = window.confirm("Вы уверены что хотите удалить?");
		if (answer) {
			removeRole(slug, auth.user.token)
				.then((res) => {
					setLoading(false);
					toast.error("Успешно удалён!");
					loadRoles();
				})
				.catch((err) => {
					if (err.response.status === 400) {
						setLoading(false);
						toast.error(err.response.data);
					}
				});
		}
	};

	// step4
	const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword);
	return (
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col">
					{loading ? (
						<h4 className="text-danger">Загрузка...</h4>
					) : (
						<h4>Создать роль</h4>
					)}

					<CategoryForm
						handleSubmit={handleSubmit}
						name={name}
						setName={setName}
						buttonName="Создать"
					/>
					{/* step2 and step3 */}
					<LocalSearch keyword={keyword} setKeyword={setKeyword} />

					{/* step5 */}
					{roles.filter(searched(keyword)).map((c) => (
						<div className="alert alert-info" key={c._id}>
							{c.name}
							<span
								onClick={() => handleRemove(c.slug)}
								className="btn btn-sm float-right"
								style={{ float: "right" }}
							>
								<DeleteOutlined className="text-danger" />
							</span>
							<Link to={`/admin/role/${c.slug}`}>
								<span
									className="btn btn-sm float-right"
									style={{ float: "right" }}
								>
									<EditOutlined className="text-warning" />
								</span>
							</Link>
						</div>
					))}
				</div>
			</div>
		</div>
	);
}
