import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import AdminNav from "components/nav/AdminNav";
import {
	createCategory,
	getCategories,
	removeCategory,
} from "../../../functions/category";
import CategoryForm from "components/forms/CategoryForm";
import LocalSearch from "components/forms/LocalSearch";

const CategoryCreate = () => {
	const { auth } = useSelector((state) => ({ ...state }));
	const [name, setName] = useState("");
	const [loading, setLoading] = useState(false);
	const [categories, setCategories] = useState([]);

	// searching filtering (step1)
	const [keyword, setKeyword] = useState("");

	useEffect(() => {
		loadCategories();
	}, []);
	const loadCategories = () =>
		getCategories().then((c) => setCategories(c.data));

	const handleSubmit = (e) => {
		e.preventDefault();
		setLoading(true);
		createCategory({ name }, auth.user.token)
			.then((res) => {
				setLoading(false);
				setName("");
				toast.success(`Категория ${res.data.name} успешно создано!`);
				loadCategories();
			})
			.catch((err) => {
				console.log(err);
				setLoading(false);
				if (err.response.status === 400) toast.error(err.response.data);
			});
	};
	const handleRemove = async (slug) => {
		setLoading(true);
		let answer = window.confirm("Вы уверены что хотите удалить?");
		if (answer) {
			removeCategory(slug, auth.user.token)
				.then((res) => {
					setLoading(false);
					toast.error("Успешно удалён!");
					loadCategories();
				})
				.catch((err) => {
					if (err.response.status === 400) {
						setLoading(false);
						toast.error(err.response.data);
					}
				});
		}
	};

	// step4
	const searched = (keyword) => (c) => c.name.toLowerCase().includes(keyword);
	return (
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col">
					{loading ? (
						<h4 className="text-danger">Загрузка...</h4>
					) : (
						<h4>Создать категорию</h4>
					)}

					<CategoryForm
						handleSubmit={handleSubmit}
						name={name}
						setName={setName}
						buttonName="Создать"
					/>
					{/* step2 and step3 */}
					<LocalSearch keyword={keyword} setKeyword={setKeyword} />

					{/* step5 */}
					{categories.filter(searched(keyword)).map((c) => (
						<div className="alert alert-info" key={c._id}>
							{c.name}
							<span
								onClick={() => handleRemove(c.slug)}
								className="btn btn-sm float-right"
								style={{ float: "right" }}
							>
								<DeleteOutlined className="text-danger" />
							</span>
							<Link to={`/admin/category/${c.slug}`}>
								<span
									className="btn btn-sm float-right"
									style={{ float: "right" }}
								>
									<EditOutlined className="text-warning" />
								</span>
							</Link>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};
export default CategoryCreate;
