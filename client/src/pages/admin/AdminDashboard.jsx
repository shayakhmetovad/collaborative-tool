import React, { useEffect } from "react";

import AdminNav from "../../components/nav/AdminNav";

const AdminDashboard = () => {
	useEffect(() => {}, []);

	return (
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col-md-10">
					<h4>Admin Dashboard</h4>
				</div>
			</div>
		</div>
	);
};
export default AdminDashboard;
