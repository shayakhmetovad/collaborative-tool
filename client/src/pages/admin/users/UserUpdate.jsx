import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Input, Select } from "antd";
import { toast } from "react-toastify";
import AdminNav from "components/nav/AdminNav";
import { updateUser, getUser } from "../../../functions/user";
import { getRoles } from "../../../functions/role";
import { useNavigate, useParams } from "react-router";
const { Option } = Select;
const UserUpdate = () => {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const [name, setName] = useState("");
	const [userData, setUserData] = useState({});
	const [roles, setRoles] = useState([]);
	const [role, setRole] = useState("");
	const [loading, setLoading] = useState(false);
	const [options, setOptions] = useState([]);
	const { id } = useParams();
	const navigate = useNavigate();
	useEffect(() => {
		loadUser();
		loadRoles();
	}, []);
	const loadUser = () => getUser(id).then((c) => setUserData(c.data));
	const loadRoles = () =>
		getRoles().then((roles) => {
			setOptions(roles.data);
		});
	const handleSubmit = (e) => {
		e.preventDefault();
		setLoading(true);
		updateUser(userData._id, { role }, user.token)
			.then((res) => {
				setLoading(false);
				toast.success(`Пользователь ${res.data.name} успешно обновлен!`);
			})
			.catch((err) => {
				setLoading(false);
				if (err.response.status === 400) toast.error(err.response.data);
			});
	};

	return (
		<div className="container-fluid">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col">
					{loading ? (
						<h4 className="text-danger">Загрузка...</h4>
					) : (
						<h4>Обновление аккаунта</h4>
					)}
					<form onSubmit={handleSubmit}>
						<label>Почта</label>
						<Input
							className="mb-2"
							disabled
							value={userData.email}
							placeholder="Почта"
						/>
						<label>Имя пользователя</label>
						<Input
							className="mb-2"
							disabled
							value={userData.username}
							placeholder="Имя пользователя"
						/>
						<label>Роль</label>
						<Select
							// mode="tags"
							allowClear
							style={{ width: "100%" }}
							placeholder="Пожалуйста выберите"
							onChange={(value) => setRole(value)}
							value={userData.role}
						>
							{options.length &&
								options.map((s) => (
									<Option key={s._id} value={s.name}>
										{s.name}
									</Option>
								))}
						</Select>
						<br />
						<button type="submit" className="btn btn-outline-primary mt-3">
							Обновить
						</button>
					</form>

					<hr />
				</div>
			</div>
		</div>
	);
};
export default UserUpdate;
