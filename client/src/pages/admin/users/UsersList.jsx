import React, { useRef, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button, Input, Space, Table } from "antd";
import {
	SearchOutlined,
	EditOutlined,
	DeleteOutlined,
} from "@ant-design/icons";
import { toast } from "react-toastify";
import Highlighter from "react-highlight-words";
import AdminNav from "components/nav/AdminNav";
import { getUsers, removeUser } from "functions/user";

export default function UsersList() {
	const { auth } = useSelector((state) => ({ ...state }));
	const [searchText, setSearchText] = useState("");
	const [searchedColumn, setSearchedColumn] = useState("");
	const [users, setUsers] = useState([]);
	const [loading, setLoading] = useState(false);
	const searchInput = useRef(null);
	useEffect(() => {
		loadUsers();
	}, []);
	const loadUsers = () => getUsers().then((c) => setUsers(c.data));
	const handleSearch = (selectedKeys, confirm, dataIndex) => {
		confirm();
		setSearchText(selectedKeys[0]);
		setSearchedColumn(dataIndex);
	};
	const handleReset = (clearFilters) => {
		clearFilters();
		setSearchText("");
	};
	const handleRemove = async (id) => {
		setLoading(true);
		let answer = window.confirm("Вы уверены что хотите удалить?");
		if (answer) {
			removeUser(id, auth.user.token)
				.then((res) => {
					setLoading(false);
					toast.error("Успешно удалён!");
					loadUsers();
				})
				.catch((err) => {
					if (err.response.status === 400) {
						setLoading(false);
						toast.error(err.response.data);
					}
				});
		}
	};
	const handleUpdate = async (id) => {
		// setLoading(true);
	};
	const getColumnSearchProps = (dataIndex) => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters,
			close,
		}) => (
			<div
				style={{
					padding: 8,
				}}
				onKeyDown={(e) => e.stopPropagation()}
			>
				<Input
					ref={searchInput}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={(e) =>
						setSelectedKeys(e.target.value ? [e.target.value] : [])
					}
					onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
					style={{
						marginBottom: 8,
						display: "block",
					}}
				/>
				<Space>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
						icon={<SearchOutlined />}
						size="small"
						style={{
							width: 90,
						}}
					>
						Search
					</Button>
					<Button
						onClick={() => clearFilters && handleReset(clearFilters)}
						size="small"
						style={{
							width: 90,
						}}
					>
						Reset
					</Button>
					<Button
						type="link"
						size="small"
						onClick={() => {
							confirm({
								closeDropdown: false,
							});
							setSearchText(selectedKeys[0]);
							setSearchedColumn(dataIndex);
						}}
					>
						Filter
					</Button>
					<Button
						type="link"
						size="small"
						onClick={() => {
							close();
						}}
					>
						close
					</Button>
				</Space>
			</div>
		),
		filterIcon: (filtered) => (
			<SearchOutlined
				style={{
					color: filtered ? "#1890ff" : undefined,
				}}
			/>
		),
		onFilter: (value, record) =>
			record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
		onFilterDropdownOpenChange: (visible) => {
			if (visible) {
				setTimeout(() => searchInput.current?.select(), 100);
			}
		},
		render: (text) =>
			searchedColumn === dataIndex ? (
				<Highlighter
					highlightStyle={{
						backgroundColor: "#ffc069",
						padding: 0,
					}}
					searchWords={[searchText]}
					autoEscape
					textToHighlight={text ? text.toString() : ""}
				/>
			) : (
				text
			),
	});
	const columns = [
		{
			title: "ФИО",
			dataIndex: "name",
			key: "fio",
			width: "30%",
			...getColumnSearchProps("fio"),
		},
		{
			title: "Username",
			dataIndex: "username",
			key: "username",
			width: "20%",
			...getColumnSearchProps("username"),
		},
		{
			title: "Mail",
			dataIndex: "email",
			key: "email",
			...getColumnSearchProps("email"),
			sorter: (a, b) => a.email.length - b.email.length,
			sortDirections: ["descend", "ascend"],
		},
		{
			title: "Роль",
			dataIndex: "role",
			key: "role",
			...getColumnSearchProps("role"),
		},
		{
			title: "Действие",
			key: "action",
			render: (_, record) => (
				<Space size="middle">
					<Link to={`/admin/user/${record._id}`} style={{ float: "right" }}>
						<EditOutlined className="text-warning" />
					</Link>
					<a
						onClick={() => handleRemove(record._id)}
						style={{ float: "right" }}
					>
						<DeleteOutlined className="text-danger" />
					</a>
				</Space>
			),
		},
	];

	// if (users && users.length > 0) {
	// 	data = users.map((item, i) => {
	// 		return {
	// 			...item,
	// 			key: i,
	// 		};
	// 	});
	// }
	return (
		<div className="container">
			<div className="row">
				<div className="col-md-2">
					<AdminNav />
				</div>
				<div className="col">
					{users.length === 0 ? (
						<p>Матрица пустая</p>
					) : (
						<Table columns={columns} dataSource={users} />
					)}
				</div>
			</div>
		</div>
	);
}
