import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Button } from "antd";
import { PlusCircleOutlined, FileWordTwoTone } from "@ant-design/icons";
import { getFilesByStatus } from "functions/document";

export default function ExpertMain() {
	const { auth } = useSelector((state) => ({ ...state }));
	const { user } = auth;
	const [files, setFiles] = useState([]);
	const navigate = useNavigate();
	useEffect(() => {
		getFilesByStatus("onExpert", auth.user.token).then((files) => {
			setFiles(files.data);
			console.log(files.data);
		});
	}, []);

	return (
		<div className="container">
			<h5 className="h5-title mb-3">Все документы</h5>
			<div className="">
				<div className="d-flex justify-content-between">
					<p className="col-1"></p>
					<p className="col-6">Название документа</p>
					<p className="col-3">Создатель документа</p>
				</div>
				{files.map((file) => (
					<div className="d-flex justify-content-between" key={file._id}>
						<FileWordTwoTone className="col-1 d-flex" />
						<p className="col-6">
							<Link to="/documents/:id">{file.title}</Link>
						</p>
						<p className="col-3">{file.createdBy.username}</p>
					</div>
				))}
			</div>
		</div>
	);
}
