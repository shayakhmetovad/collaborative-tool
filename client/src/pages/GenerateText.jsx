import React, { useEffect, useState } from "react";
import { Button, Input, Typography } from "antd";
import axios from "axios";
import { sendText } from "../api/textApi";
import "./inputText.scss";
import FunctionalMenu from "components/nav/FunctionalMenu";
const { TextArea } = Input;
const { Title } = Typography;

const InputText = () => {
	const [loading, setLoading] = useState(false);
	const [loadingImage, setLoadingImage] = useState(false);
	const [text, setText] = useState("");
	const [textImage, setTextImage] = useState("");
	const [image, setImage] = useState("");
	const [summary, setSummary] = useState("");
	const [error, setError] = useState("");
	const [errorImage, setErrorImage] = useState("");
	const [reportFetched, setReportFetched] = useState(false);

	useEffect(() => {
		setReportFetched(false);
		setSummary("");
		setImage("");
	}, []);
	function inputChangeHandle(event) {
		setText(event.target.value);
	}
	function inputChangeImageHandle(event) {
		setTextImage(event.target.value);
	}
	const handleSubmit = async (e) => {
		e.preventDefault();
		setLoading(true);
		try {
			const { data } = await axios.post(
				`${process.env.REACT_APP_API}/paragraph`,
				{ text }
			);
			setSummary(data);
		} catch (err) {
			if (err.response.data.error) {
				setError(err.response.data.error);
			} else if (err.message) {
				setError(err.message);
			}
			setTimeout(() => {
				setError("");
			}, 5000);
		}
		setReportFetched(true);
		setLoading(false);
	};
	const handleSubmitPhoto = async (e) => {
		e.preventDefault();
		setLoadingImage(true);
		try {
			const { data } = await axios.post(
				`${process.env.REACT_APP_API}/scifi-image`,
				{ textImage }
			);
			setImage(data);
		} catch (err) {
			if (err.response.data.error) {
				setErrorImage(err.response.data.error);
			} else if (err.message) {
				setErrorImage(err.message);
			}
			setTimeout(() => {
				setErrorImage("");
			}, 5000);
		}
		setLoadingImage(false);
	};
	var loader = (
		<div className="loader">
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
		</div>
	);
	var loaderImage = (
		<div className="loader">
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
			<span className="circle"></span>
		</div>
	);

	return (
		<div className="d-flex">
			<FunctionalMenu />

			<div className="container">
				<form onSubmit={handleSubmit}>
					<Title level={4}>Сгенерировать текст</Title>

					<TextArea
						rows={1}
						placeholder="Введите текст"
						value={text}
						onChange={inputChangeHandle}
					/>
					<br />
					<br />
					{!loading && (
						<Button
							type="primary"
							disabled={!text}
							htmlType="submit"
							className="submitButton"
							// onClick={handleSubmit}
						>
							Сгенерировать
						</Button>
					)}
					{loading && (
						<Button type="primary" className="submitButton">
							{loader}
						</Button>
					)}
					{reportFetched && summary && (
						<Typography p={1}>Итог : {summary}</Typography>
					)}
				</form>
				<br />
				<br />
				<form onSubmit={handleSubmitPhoto}>
					<Title level={4}>Сгенерировать фото</Title>
					<TextArea
						rows={1}
						placeholder="Введите текст"
						value={textImage}
						onChange={inputChangeImageHandle}
					/>
					<br />
					<br />
					{!loadingImage && (
						<Button
							type="primary"
							disabled={!text}
							htmlType="submit"
							className="submitButton"
							// onClick={handleSubmit}
						>
							Сгенерировать
						</Button>
					)}
					{loadingImage && (
						<Button type="primary" className="submitButton">
							{loaderImage}
						</Button>
					)}
					{image && (
						<Typography p={2}>
							Фото : <img src={image} alt="scifiimage" />
						</Typography>
					)}
				</form>
			</div>
		</div>
	);
};

export default InputText;
