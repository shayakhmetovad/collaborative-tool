import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Form, Input, Button, Select, DatePicker } from "antd";
import { toast } from "react-toastify";
import { createFile } from "functions/document";
import { getCategories } from "functions/category";
const { RangePicker } = DatePicker;
const { TextArea } = Input;

const { Option } = Select;
export default function CreateDocument() {
	const { auth } = useSelector((state) => ({ ...state }));
	const [loading, setLoading] = useState(false);
	const [options, setOptions] = useState([]);
	const [values, setValues] = useState({
		title: "",
		description: "",
		categories: [],
	});
	let navigate = useNavigate();
	const { title, description, categories } = values;
	const loadCategories = () =>
		getCategories().then((categories) => {
			setOptions(categories.data);
		});
	useEffect(() => {
		loadCategories();
	}, []);

	const handleChange = (e) => {
		setValues({ ...values, [e.target.name]: e.target.value });
	};
	const handleSubmit = (e) => {
		console.log(values, auth.user.token);
		setLoading(true);
		createFile(values, auth.user.token)
			.then((res) => {
				console.log(res);
				setLoading(false);
				toast.success(`Файл создан!`);
				navigate("/home");
			})
			.catch((err) => {
				console.log(err);
				setLoading(false);
				// if (err.response.status === 400) toast.error(err.response.data);
			});
	};
	return (
		<div className="container">
			<h3 className="text-center">Создать книгу</h3>
			<Form layout="vertical" onFinish={handleSubmit}>
				<Form.Item label="Название книги">
					<Input value={title} onChange={handleChange} name="title" />
				</Form.Item>
				<Form.Item label="Описание">
					<TextArea
						rows={4}
						value={description}
						onChange={handleChange}
						name="description"
					/>
				</Form.Item>
				<Form.Item label="Категория">
					<Select
						mode="tags"
						allowClear
						style={{ width: "100%" }}
						placeholder="Пожалуйста выберите"
						onChange={(value) => setValues({ ...values, categories: value })}
						value={categories}
					>
						{options.length &&
							options.map((s) => (
								<Option key={s._id} value={s._id}>
									{s.name}
								</Option>
							))}
					</Select>
				</Form.Item>

				<Button type="primary" htmlType="submit">
					Создать
				</Button>
			</Form>
		</div>
	);
}
