import isEmpty from "../validation/is-empty";
const initialState = {
	isAuthenticated: false,
	user: {},
	currentUserId: null,
};

export const userReducer = (state = initialState, action) => {
	switch (action.type) {
		case "SET_CURRENT_USER":
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				user: action.payload,
				currentUserId: action.payload.id,
			};
		case "LOGOUT":
			return action.payload;
		default:
			return state;
	}
};
