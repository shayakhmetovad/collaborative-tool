import axios from "axios";

export const createFile = async (file, authtoken) =>
	await axios.post(`${process.env.REACT_APP_API}/file`, file, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
export const getFiles = async (authtoken) =>
	await axios.get(`${process.env.REACT_APP_API}/files`, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
export const getFilesByStatus = async (status, authtoken) =>
	await axios.get(`${process.env.REACT_APP_API}/filesByStatus/${status}`, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
export const getDoc = async (id) => {
	return await axios.get(`${process.env.REACT_APP_API}/doc/${id}`);
};
export const getMessages = async (id) => {
	return await axios.get(`${process.env.REACT_APP_API}/messages/${id}`);
};
