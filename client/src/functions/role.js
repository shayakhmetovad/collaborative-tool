import axios from "axios";

export const getRoles = async () => {
	return await axios.get(`${process.env.REACT_APP_API}/roles`);
};

export const getRole = async (slug) => {
	return await axios.get(`${process.env.REACT_APP_API}/role/${slug}`);
};

export const removeRole = async (slug, authtoken) => {
	return await axios.delete(`${process.env.REACT_APP_API}/role/${slug}`, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
};

export const updateRole = async (slug, role, authtoken) =>
	await axios.put(`${process.env.REACT_APP_API}/role/${slug}`, role, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});

export const createRole = async (data, authtoken) =>
	await axios.post(`${process.env.REACT_APP_API}/role`, data, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
