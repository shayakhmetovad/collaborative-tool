import axios from "axios";

export const getUsers = async () => {
	return await axios.get(`${process.env.REACT_APP_API}/users`);
};

export const getUser = async (id) => {
	return await axios.get(`${process.env.REACT_APP_API}/user/${id}`);
};

export const removeUser = async (id, authtoken) => {
	return await axios.delete(`${process.env.REACT_APP_API}/user/${id}`, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
};

export const updateUser = async (id, user, authtoken) =>
	await axios.put(`${process.env.REACT_APP_API}/user/${id}`, user, {
		headers: {
			authorization: "Bearer " + authtoken,
		},
	});
