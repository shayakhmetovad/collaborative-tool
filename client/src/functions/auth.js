import axios from "axios";
import setAuthToken from "utils/setAuthToken";

export const createOrUpdateUser = async (authToken) => {
	return await axios.post(
		`${process.env.REACT_APP_API}/create-or-update-user`,
		{},
		{
			headers: {
				authToken: authToken,
			},
		}
	);
};
export const currentUser = async (authToken) => {
	return await axios.post(
		`${process.env.REACT_APP_API}/current-user`,
		{},
		{
			headers: {
				authToken: authToken,
			},
		}
	);
};
export const currentAdmin = async () => {
	const token = JSON.parse(localStorage.getItem("token"));
	return await axios.post(
		`${process.env.REACT_APP_API}/current-admin`,
		{},
		{
			headers: {
				authorization: "Bearer " + token,
			},
		}
	);
};
export const currentAdviser = async () => {
	const token = JSON.parse(localStorage.getItem("token"));
	return await axios.post(
		`${process.env.REACT_APP_API}/current-adviser`,
		{},
		{
			headers: {
				authorization: "Bearer " + token,
			},
		}
	);
};
export const currentEditor = async () => {
	const token = JSON.parse(localStorage.getItem("token"));
	return await axios.post(
		`${process.env.REACT_APP_API}/current-editor`,
		{},
		{
			headers: {
				authorization: "Bearer " + token,
			},
		}
	);
};
export const currentExpert = async () => {
	const token = JSON.parse(localStorage.getItem("token"));
	return await axios.post(
		`${process.env.REACT_APP_API}/current-expert`,
		{},
		{
			headers: {
				authorization: "Bearer " + token,
			},
		}
	);
};
export const register = async (user) => {
	await axios.post(`${process.env.REACT_APP_API}/register`, user);
};
export const login = async (user) => {
	return await axios.post(`${process.env.REACT_APP_API}/login`, user);
};
export const setCurrentUser = (decoded) => {
	return {
		type: "SET_CURRENT_USER",
		payload: decoded,
	};
};
export const logoutUser = () => {
	localStorage.removeItem("token");
	localStorage.removeItem("auth");
	setAuthToken(false);
	// set current user to {} which will set isAuthenticated to false
	return {
		type: "SET_CURRENT_USER",
		payload: {},
	};
};
